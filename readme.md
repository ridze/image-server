# Static Files Server
Serving file names in json format and files as static (at the moment only one array of images)

### Installing

Instructions to install and run the project:

Add environment variables:
```
create .env in project root with the following data:

    NODE_ENV=dev
    PORT=3001 (or other)
```

Install dependencies
```
yarn install
```

Start server
```
yarn start
```

### How to use

```
add file names to src/constants
```

```
add files to src/public
```

```
use get method on {DOMAIN_NAME}/api/slide-show to get file names
```

```
use files from {DOMAIN_NAME}/{PATH_TO_FILE}/{FILE_NAME} or wherever you want to put them
NOTE: PATH_TO_FILE does not include public folder
```
