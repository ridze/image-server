const dotenv = require('dotenv');

const config = {};
if (process.env.NODE_ENV) {
	config.path = `./.env.${process.env.NODE_ENV}`;
	console.log(`Loading env configuration for ${process.env.NODE_ENV}`);
} else {
	console.log('Loading default env configuration');
}

dotenv.config(config);

module.exports = dotenv;
