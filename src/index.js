require('./core/config'); // load dotenv

const express = require('express');
const bodyParser = require('body-parser');

const static = require('path').join(__dirname,'/public');

const app = express();

app.use(bodyParser.json());
app.use(express.static(static));

app.use((req, res, next) => {
	const {
		origin,
	} = req.headers;

	res.set({
		'Access-Control-Allow-Origin': origin, // specifying an explicit origin allow to pass credentials as well, something a '*' doesn't allow
		'Access-Control-Allow-Credentials': 'true',
		'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT,PATCH,DELETE',
		'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
	});

	return next(null);
});

// routes
app.use(require('./api/routes'));

app.listen(process.env.PORT, () => {
	console.log(`App started, port: ${process.env.PORT}, running as: ${process.env.NODE_ENV}`);
});

module.exports = app;
