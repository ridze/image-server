const express = require('express');

const router = express.Router();

// slide-show
router.use('/api', require('./slideshow'));

module.exports = router;
