const express = require('express');
const { slideShow } = require('../constants');

const SlideShowRouter = express.Router();

SlideShowRouter.get('/slide-show', async (req, res) => {
	res.send({ slideShow });
});

module.exports = SlideShowRouter;
